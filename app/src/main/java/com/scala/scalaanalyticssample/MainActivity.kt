package com.scala.scalaanalyticssample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.scala.scalaanalytics.ScalaAnalyticsClient
import com.scala.scalaanalytics.data.ScalaAnalyticsMetadata
import com.scala.scalaanalytics.data.map.*
import com.scala.scalaanalytics.utils.PageType
import com.scala.scalaanalyticssample.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initScalaAnalyticsClient()

        setUpBindings()
    }

    private lateinit var scalaAnalyticsClient: ScalaAnalyticsClient
    private var apiHost: String = BuildConfig.SCALA_API_HOST_VALUE
    private var apiKey: String = BuildConfig.SCALA_API_KEY_VALUE


    private fun initScalaAnalyticsClient() {
        scalaAnalyticsClient = ScalaAnalyticsClient(applicationContext, apiHost, apiKey)
    }

    private fun setUpBindings() {
        binding.btnPages.setOnClickListener { sendPageData() }
        binding.btnSearch.setOnClickListener { sendSearchData() }
        binding.btnEcommPdp.setOnClickListener { sendPDPData() }
        binding.btnOrder.setOnClickListener { sendOrderData() }
        binding.btnCampaign.setOnClickListener { sendCampaingData() }
        binding.btnProfile.setOnClickListener { sendProfileData() }
        binding.btnUtm.setOnClickListener { sendUTMData() }
        binding.btnEvent.setOnClickListener { sendEventData() }
        binding.btnShipping.setOnClickListener { sendShippingData() }
        binding.btnWishlist.setOnClickListener { sendWishlistData() }
    }

    private fun sendPageData() {

        val metadata = ScalaAnalyticsMetadata()
        val page = ScalaAnalyticsPage()
        page.pagepath = "TESTE Android https://esportes.scala.com.br/busca?q=cadar%C3%A7o"
        page.pagetype = PageType.NAVEGACAO
        page.hostname = "Nome do Host"
        page.pagetitle = "Esportes"
        page.previouspage = PageType.LANDING_PAGE

        page.pageview = "pageview"
        metadata.page = page

        scalaAnalyticsClient.track(metadata)
    }

    private fun sendSearchData() {

        val metadata = ScalaAnalyticsMetadata()
        val search = ScalaAnalyticsSearch()
        search.key = "Tenis nike"
        search.category = "Calçados"
        search.group = "Calçados"
        search.results = "10"
        search.subcategory = "Corrida / caminhada"

        metadata.search = search

        scalaAnalyticsClient.track(metadata)
    }

    private fun sendPDPData() {

        val metadata = ScalaAnalyticsMetadata()
        val page = ScalaAnalyticsPage()
        page.pagepath =
            "TESTE Android https://m.scala.com.br/bermuda-fatal-tecnologico-9921-masculina-926771.html?cor=2V"
        page.pagetype = PageType.DETALHE_PRODUTO
        metadata.page = page
        val pdp = ScalaAnalyticsPDP()
        pdp.sku = "92677103"
        pdp.name = "Bermuda Fatal Tecnológico 9921 - Masculina"
        pdp.category = "Casual"
        pdp.price = "69,99"
        pdp.stockt = "Normal"
        pdp.stocko = "CD"
        pdp.available = "True"

        metadata.pdp = pdp

        scalaAnalyticsClient.track(metadata)
    }

    private fun sendOrderData() {

        val metadata = ScalaAnalyticsMetadata()
        val order = ScalaAnalyticsOrder()
        val cart = ScalaAnalyticsCartItem()
        cart.sku = "849250330389"
        cart.description =
            "Patins Oxer Freestyle - In Line - Freestyle / Slalom - ABEC 9 - Base de Alumínio"
        cart.category = "Skating"
        cart.price = "329,99"
        cart.quantity = "1"
        cart.stocko = "CD"
        cart.fretev = "10,00"
        cart.fretep = "9 dias úteis"
        val cartitem2 = ScalaAnalyticsCartItem()
        cartitem2.sku = "849250930300"
        cartitem2.description = "PTESTE"
        cartitem2.category = "Skating"
        cartitem2.price = "329,99"
        cartitem2.quantity = "1"
        cartitem2.stocko = "CD"
        cartitem2.fretev = "10,00"
        cartitem2.fretep = "9 dias úteis"

        order.tax = "10,00"
        order.revenue = "10000,00"
        order.postalcode = "22221-111"
        order.transactionId = "transactionId"

        val listCartItems = ArrayList<ScalaAnalyticsCartItem>()
        listCartItems.add(cart)
        listCartItems.add(cartitem2)
        listCartItems.add(cart)

        metadata.order = order
        metadata.cart = listCartItems

        scalaAnalyticsClient.track(metadata)
    }

    private fun sendProfileData() {

        val metadata = ScalaAnalyticsMetadata()
        val profile = ScalaAnalyticsProfile()
        profile.id = "145902417"
        profile.state = "PE"
        profile.gender = "Feminino"
        profile.age = "39"
        profile.postalcode = "22222-222"
        profile.language = "PT-BR"

        metadata.profile = profile

        scalaAnalyticsClient.track(metadata)
    }

    private fun sendCampaingData() {

        val metadata = ScalaAnalyticsMetadata()
        val campaign = ScalaAnalyticsCampaign()
        campaign.name = "Nome Campanha"
        metadata.campaign = campaign

        scalaAnalyticsClient.track(metadata)
    }

    private fun sendWishlistData() {
        val metadata = ScalaAnalyticsMetadata()
        val wishList = ArrayList<ScalaAnalyticsWishlist>()
        wishList.add(ScalaAnalyticsWishlist(sku="849250930300", price="10,00"))
        wishList.add(ScalaAnalyticsWishlist(sku="849250930301", price="20,00"))
        wishList.add(ScalaAnalyticsWishlist(sku="849250930302", price="30,00"))

        metadata.wishlist = wishList

        scalaAnalyticsClient.track(metadata)
    }

    private fun sendUTMData() {
        val metadata = ScalaAnalyticsMetadata()
        val utm = ScalaAnalyticsUTM()

        utm.medium = "3451609"
        utm.referralPath = "google"
        utm.campaign = "Parcerias Rakuten 1234"
        utm.source = "parcerias_rakuten"
        utm.medium = "3451609"
        utm.keyword = "Rakuten123"

        metadata.utm = utm


        scalaAnalyticsClient.track(metadata)
    }

    private fun sendEventData() {
        val metadata = ScalaAnalyticsMetadata()
        val event = ScalaAnalyticsEvent()
        event.action = "Type - Buscar um produto"
        event.name = "Busca por item: Camisa da c"
        event.category = "UI - Header - Busca"
        event.value = "0"

        metadata.event = event

        scalaAnalyticsClient.track(metadata)
    }

    private fun sendShippingData() {
        val metadata = ScalaAnalyticsMetadata()
        val shipping = ArrayList<ScalaAnalyticsShippingItem>()

        val item1 = ScalaAnalyticsShippingItem()
        val item2 = ScalaAnalyticsShippingItem()

        val tipoe1 =
            ScalaAnalyticsShippingItem.Tipoe( frete="Entrega Normal",  nome="nome", prazo="10,00")
        val tipoe2 = ScalaAnalyticsShippingItem.Tipoe("Receba Amanhã", "", "30,00")

        val listtipoe = ArrayList<ScalaAnalyticsShippingItem.Tipoe?>()
        listtipoe.add(tipoe1)
        listtipoe.add(tipoe2)

        item1.id = "1"
        item1.sku = "M0075S030001"
        item1.qtd = "1"
        item1.tipoe = listtipoe

        item2.id = "1"
        item2.sku = "M00762070005"
        item2.qtd = "2"
        item2.tipoe = listtipoe

        shipping.add(item1)
        shipping.add(item2)


        metadata.shipping = shipping

        scalaAnalyticsClient.track(metadata)
    }


}