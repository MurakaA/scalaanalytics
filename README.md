# ScalaAnalytics for Android

This is a sample App that imports and uses ScalaAnalytics library



## Installing ScalaAnalytics
ScalaAnalytics is imported using an .aar file, using latest Android Studio Version (Arctic Fox) provided by ScalaAnalytics 

Add .aar file to a package named /scalaanalyticslibrary folder
like this sample app:

https://gitlab.com/MurakaA/scalaanalytics/-/blob/master/scalaanalyticslibrary/scalaanalyticslibrary-1.0.8.aar

create a build.gradle file on the same library folder, with the same content as:
https://gitlab.com/MurakaA/scalaanalytics/-/blob/master/scalaanalyticslibrary/build.gradle

`````
configurations.maybeCreate("default")
artifacts.add("default", file('scalaanalyticslibrary-1.0.8.aar'))
`````



on app/build.gradle,
import library reference:

https://gitlab.com/MurakaA/scalaanalytics/-/blob/master/app/build.gradle#L60-61


````
    // scala libs dependencies TODO: include these on your project
    implementation(project(path: ":scalaanalyticslibrary"))
````





LICENSE
---
Distributed under the MIT License.
